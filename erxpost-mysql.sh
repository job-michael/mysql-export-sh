#! /bin/bash

TIMESTAMP=$(date +"%F")
BACKUP_DIR="/home/username/mysql-export/$TIMESTAMP"
MYSQL_USER="YOUR-LOGIN"
MYSQL=/usr/bin/mysql
MYSQL_PASSWORD="YOUR-PWD"
MYSQLDUMP=/usr/bin/mysqldump

mkdir -p "$BACKUP_DIR/mysql"

databases=`$MYSQL --user=$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema|performance_schema)"`

for db in $databases; do
  $MYSQLDUMP --force --routines --opt --user=$MYSQL_USER -p$MYSQL_PASSWORD --databases $db | gzip > "$BACKUP_DIR/mysql/$db.gz"
done